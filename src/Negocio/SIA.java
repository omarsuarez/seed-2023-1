/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Estudiante;
import Util.seed.ArchivoLeerURL;
import Util.seed.ListaCD;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DOCENTE
 */
public class SIA {

    private ListaCD<Estudiante> fisica;
    private ListaCD<Estudiante> estructuras;
    private ListaCD<Estudiante> poo;

    public SIA(String urlFisica, String urlEstructuras, String urlPoo) {
        this.fisica = crear(urlFisica);
        this.estructuras = crear(urlEstructuras);
        this.poo = crear(urlPoo);
    }

    private ListaCD<Estudiante> crear(String url) {
        ListaCD<Estudiante> l = new ListaCD();
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object linea[] = archivo.leerArchivo();
        for (int i = 1; i < linea.length; i++) {
            String fila = linea[i].toString();
            //16980,NOMBRE, materia 1,0,2,4,4
            String datos[] = fila.split(",");
            long codigo = Long.parseLong(datos[0]);
            String nombre = datos[1];
            String materia = datos[2];
            float p1 = Float.parseFloat(datos[3]);
            float p2 = Float.parseFloat(datos[4]);
            float p3 = Float.parseFloat(datos[5]);
            float exm = Float.parseFloat(datos[6]);
            Estudiante nuevo = new Estudiante(codigo, nombre, materia, p1, p2, p3, exm);
            l.insertarFinal(nuevo);
        }
        return l;
    }

    public String getListadoFisica() {
        String msg = "Listado Física \n";
        for (Estudiante dato : this.fisica) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }
    
    public String getListadoEstructuras() {
        String msg = "Listado Estructuras \n";
        for (Estudiante dato : this.estructuras) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }
    
    public String getListadoPoo() {
        String msg = "Listado Poo \n";
        for (Estudiante dato : this.poo) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public ListaCD<String> getListadoFinal() {
        ListaCD<String> resultado = new ListaCD();
        for (Estudiante x : this.fisica) {
            String m = (x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getPromedio() + "-" + m+"\n");
        }
        
        for (Estudiante x : this.estructuras) {
            String m = (x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getPromedio() + "-" + m+"\n");
        }
        
        for (Estudiante x : this.poo) {
            String m = ( x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getPromedio() + "-" + m+"\n");
        }
        
        return resultado;
    }
    
    public void getInformePDF()
    {
        try {
            new ImpresoraInformeSIA().imprimirListadoPromedio(this.getListadoFinal());
        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(SIA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
public void crearPdf() throws Exception{
        Document documento = new Document();
        
        FileOutputStream ficheroPdf = new FileOutputStream("Estudiantes.pdf");
        
        PdfWriter.getInstance(documento, ficheroPdf);
        
        documento.open();
        
        Paragraph titulo = new Paragraph("Listado estudiantes \n\n");
        
        PdfPTable tabla = new PdfPTable(3);
        tabla.addCell("Codigo");
        tabla.addCell("Nombre");
        tabla.addCell("Materia");
        
        for(Estudiante x : this.poo && this.estructuras && this.fisica){
            this.getListadoFinal().toString();
        }
        
        documento.close();
        
    }


}
