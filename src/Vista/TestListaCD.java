/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaCD;

/**
 *
 * @author docente
 */
public class TestListaCD {
    
    public static void main(String[] args) {
        
        ListaCD<Integer> lista = new ListaCD();
        lista.insertarInicio(5);
        lista.insertarInicio(3);
        lista.insertarInicio(9);
        System.out.println(lista.toString());
        System.out.println("Borrando pos:1, la lista queda:");
        lista.remove(1);
        System.out.println(lista.toString());
        ListaCD<Integer> lista2 = new ListaCD();
        lista2.insertarFinal(5);
        lista2.insertarFinal(3);
        lista2.insertarFinal(9);
        System.out.println(lista2.toString());   
        System.out.println(lista2.containTo(9)); 
        System.out.println(lista2.containTo(20)); 
     
        
        
    }
    
}
