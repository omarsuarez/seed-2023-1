/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;
import Util.seed.ArchivoLeerURL;
/**
 *
 * @author DOCENTE
 */
public class TestLeerURL {
    
    public static void main(String[] args) {
        ArchivoLeerURL archivo= new ArchivoLeerURL("https://gitlab.com/pruebas_madarme/persistencia/ed_a/sia-a/-/raw/main/notas0a5.csv?ref_type=heads");
        Object linea[]=archivo.leerArchivo();
        for(Object dato:linea)
            System.out.println(dato.toString());
    }
    
}
