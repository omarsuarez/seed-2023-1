/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaS;

/**
 *
 * @author Docente
 */
public class TestListaS {

    /**
     * YO SOY EL DESARROLLADOR QUE USA LA ESTRUCTURA
     *
     * @param args
     */
    public static void main(String[] args) {
        ListaS<Integer> l = new ListaS();

        l.insertarInicio(2);
        l.insertarInicio(4);
        l.insertarInicio(5);
        l.insertarFin(23);
        l.insertarFin(17);

        System.out.println(l.toString());
        //5-4-2-23-17
        for (int i = 0; i < l.getSize(); i++) {
            int cuadrado = l.get(i);
            cuadrado *= cuadrado;
            l.set(i, cuadrado);
            //Si fuese un Vector: l[i]=l[i]*l[i]
        }
        System.out.println(l.toString());

        //Propicio un error:
        try {
            System.out.println(l.get(100));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
